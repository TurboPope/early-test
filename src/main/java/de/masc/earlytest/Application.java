package de.masc.earlytest;

import com.squareup.okhttp.OkHttpClient;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.http.GET;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.Base64;

public class Application {
    private interface EarlyService {
        @GET("/res")
        String getResource();
    }

    public static void main(String[] args) throws NoSuchAlgorithmException, KeyStoreException, UnrecoverableKeyException, KeyManagementException, IOException, CertificateException {
        // Self-Signed Certificate created for localhost with: http://www.selfsignedcertificate.com/
        // Keystore reference: https://www.digitalocean.com/community/tutorials/java-keytool-essentials-working-with-java-keystores
        // OkHttp use keystore: http://stackoverflow.com/a/24401795/4112204

        OkHttpClient client = new OkHttpClient();

        KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
        char[] keystorePassword = "SOMEPW".toCharArray();
        FileInputStream fileInputStream = new FileInputStream("cert/self-signed/keystore.jks");
        keyStore.load(fileInputStream, keystorePassword);
        fileInputStream.close();

        SSLContext sslContext = SSLContext.getInstance("SSL");
        TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        trustManagerFactory.init(keyStore);
        KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        keyManagerFactory.init(keyStore, null);
        sslContext.init(keyManagerFactory.getKeyManagers(), trustManagerFactory.getTrustManagers(), new SecureRandom());
        client.setSslSocketFactory(sslContext.getSocketFactory());

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setClient(new OkClient(client))
                .setEndpoint("https://localhost:8080")
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        final String credentials = "testuser" + ":" + "testpassword";
                        final String authHeader = "Basic " + Base64.getEncoder().encodeToString(credentials.getBytes());
                        System.out.println("authHeader = " + authHeader);
                        request.addHeader("Accept", "application/json");
                        request.addHeader("Authorization", authHeader);
                    }
                })
                .build();
        EarlyService early = restAdapter.create(EarlyService.class);

        System.out.println(early.getResource());
    }
}
